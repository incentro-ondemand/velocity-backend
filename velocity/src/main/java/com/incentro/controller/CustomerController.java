package com.incentro.controller;

import com.incentro.service.CartService;
import com.incentro.service.CustomerService;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.models.Address;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;
import static com.incentro.service.CustomerService.setDefaultShippingAddress;

@RestController
public class CustomerController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/customer/create", method = RequestMethod.GET)
    public ResponseEntity createCustomer(@RequestParam String firstName, String lastName, String email, String password) {
        try (final SphereClient client = createSphereClient()) {

            Customer customer = CustomerService.createCustomer(firstName, lastName, email, password, client);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>("Email already in use", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/customer/login", method = RequestMethod.GET)
    public ResponseEntity loginCustomer(@RequestParam String customerEmail,
                                        @RequestParam String customerPassword,
                                        @RequestParam(required = false) String anonymousCartId) {
        try (final SphereClient client = createSphereClient()) {
            if (anonymousCartId == null) {
                final Customer customer = CustomerService.loginCustomer(client, customerEmail, customerPassword).toCompletableFuture().get().getCustomer();
                return new ResponseEntity<>(customer, HttpStatus.OK);
            } else {
                final Customer customer = CustomerService.loginCustomerWithAnonymousCart(client, customerEmail, customerPassword, anonymousCartId).toCompletableFuture().get().getCustomer();
                return new ResponseEntity<>(customer, HttpStatus.OK);
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>("Invalid email or password", HttpStatus.NOT_FOUND);
        }
    }


    @RequestMapping(value = "/customer/get", method = RequestMethod.GET)
    public ResponseEntity getCustomer(@RequestParam String customerId) {
        try (final SphereClient client = createSphereClient()) {
            return new ResponseEntity<>(CustomerService.getCustomer(client, customerId).toCompletableFuture().get(), HttpStatus.OK);
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/customer/cart", method = RequestMethod.GET)
    public ResponseEntity getCartByCustomer(@RequestParam String customerId) {
        try (final SphereClient client = createSphereClient()) {
            return new ResponseEntity<>(CartService.buildCart(CustomerService.getCartByCustomer
                    (client, customerId).toCompletableFuture().get()), HttpStatus.OK);
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "customer/name", method = RequestMethod.GET)
    public ResponseEntity changeCustomerName(@RequestParam String customerId,
                                             @RequestParam(required = false) String title,
                                             @RequestParam(required = false) String firstName,
                                             @RequestParam(required = false) String lastName) {
        try (final SphereClient client = createSphereClient()) {
            final Customer customer = CustomerService.getCustomer(client, customerId).toCompletableFuture().get();
            return new ResponseEntity<>(CustomerService.changeName(client, customer, title, firstName, lastName)
                    .toCompletableFuture().get(), HttpStatus.OK);
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>("Failed to update customer name", HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "customer/address/add", method = RequestMethod.GET)
    public ResponseEntity addAddress(@RequestParam String customerId,
                                     @RequestParam String countryCode,
                                     @RequestParam(required = false) String city,
                                     @RequestParam(required = false) String region,
                                     @RequestParam(required = false) String state,
                                     @RequestParam(required = false) String streetName,
                                     @RequestParam(required = false) String streetNumber,
                                     @RequestParam String postalCode,
                                     @RequestParam(required = false) String firstName,
                                     @RequestParam(required = false) String lastName,
                                     @RequestParam(required = false) String salutation,
                                     @RequestParam(required = false) String title,
                                     @RequestParam(required = false) String company,
                                     @RequestParam(required = false) String department,
                                     @RequestParam(required = false) String email,
                                     @RequestParam(required = false) String phone,
                                     @RequestParam(required = false) String mobile,
                                     @RequestParam(required = false) String fax,
                                     @RequestParam boolean setAsDefaultBillingAndShippingAddress) {
        try (final SphereClient client = createSphereClient()) {
            final Customer customer = CustomerService.getCustomer(client, customerId).toCompletableFuture().get();
            List<Address> addresses = customer.getAddresses();
            if (setAsDefaultBillingAndShippingAddress) {
                if (addresses.isEmpty()) {
                    final Address address = CustomerService.addAddress(client, customer, countryCode, city, region, state, streetName, streetNumber, postalCode, firstName,
                            lastName, salutation, title, company, department, email, phone, mobile, fax)
                            // TODO add to service methods
                            .thenComposeAsync(updatedCustomer -> CustomerService.setDefaultShippingAddress(client, updatedCustomer)
                            .thenComposeAsync(updatedCustomer1 -> CustomerService.setDefaultSBillingAddress(client, updatedCustomer1)))
                            .toCompletableFuture().get().getAddresses().get(0);
                    return new ResponseEntity<>(address, HttpStatus.OK);
                } else {
                    final Customer newCustomer = CustomerService.addAddress(client, customer, countryCode, city, region, state, streetName, streetNumber, postalCode, firstName,
                            lastName, salutation, title, company, department, email, phone, mobile, fax)
                            .thenComposeAsync(updatedCustomer -> setDefaultShippingAddress(client,updatedCustomer, postalCode))
                            .thenComposeAsync(updatedCustomer -> CustomerService.setDefaultBillingAddress(client, updatedCustomer, postalCode))
                            .toCompletableFuture().get();
                    LinkedList<Address> updatedAddresses = new LinkedList<>(newCustomer.getAddresses());
                    return new ResponseEntity(updatedAddresses.getLast(), HttpStatus.OK);
                }
            } else {
                if (addresses.isEmpty()) {
                    final Address address = CustomerService.addAddress(client, customer, countryCode, city, region, state, streetName, streetNumber, postalCode, firstName,
                            lastName, salutation, title, company, department, email, phone, mobile, fax).toCompletableFuture().get().getAddresses().get(0);
                    return new ResponseEntity<>(address, HttpStatus.OK);
                } else {
                    final Customer newCustomer = CustomerService.addAddress(client, customer, countryCode, city, region, state, streetName, streetNumber, postalCode, firstName,
                            lastName, salutation, title, company, department, email, phone, mobile, fax).toCompletableFuture().get();
                    LinkedList<Address> updatedAddresses = new LinkedList<>(newCustomer.getAddresses());
                    return new ResponseEntity(updatedAddresses.getLast(), HttpStatus.OK);
                }
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity("Address not added", HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "customer/address/update", method = RequestMethod.GET)
    public ResponseEntity updateAddress(@RequestParam String addressId,
                                        @RequestParam String customerId,
                                        @RequestParam String countryCode,
                                        @RequestParam(required = false) String city,
                                        @RequestParam(required = false) String region,
                                        @RequestParam(required = false) String state,
                                        @RequestParam(required = false) String streetName,
                                        @RequestParam(required = false) String streetNumber,
                                        @RequestParam(required = false)String postalCode,
                                        @RequestParam(required = false) String firstName,
                                        @RequestParam(required = false) String lastName,
                                        @RequestParam(required = false) String salutation,
                                        @RequestParam(required = false) String title,
                                        @RequestParam(required = false) String company,
                                        @RequestParam(required = false) String department,
                                        @RequestParam(required = false) String email,
                                        @RequestParam(required = false) String phone,
                                        @RequestParam(required = false) String mobile,
                                        @RequestParam(required = false) String fax,
                                        @RequestParam boolean setAsDefaultBillingAndShippingAddress) {
        try (final SphereClient client = createSphereClient()) {
            final Customer customer = CustomerService.getCustomer(client, customerId).toCompletableFuture().get();
            if (setAsDefaultBillingAndShippingAddress) {
                Address oldAddress = customer.getAddressById(addressId);
                Address newAddress = CustomerService.updateAddress(client,customer, oldAddress, countryCode, city, region, state, streetName, streetNumber, postalCode,
                        firstName, lastName, salutation, title, company, department, email, phone, mobile, fax)
                        .thenComposeAsync(updatedCustomer -> CustomerService.setDefaultShippingAddressWithAddressId(client, updatedCustomer, addressId))
                        .thenComposeAsync(updatedCustomer -> CustomerService.setDefaultBillingAddressWithAddressId(client, updatedCustomer, addressId))
                        .toCompletableFuture().get().getAddressById(addressId);
                return new ResponseEntity<>(newAddress, HttpStatus.OK);
            } else {
                Address oldAddress = customer.getAddressById(addressId);
                Address newAddress = CustomerService.updateAddress(client, customer, oldAddress, countryCode, city, region, state, streetName, streetNumber, postalCode,
                        firstName, lastName, salutation, title, company, department, email, phone, mobile, fax).toCompletableFuture().get().getAddressById(addressId);
                return new ResponseEntity<>(newAddress, HttpStatus.OK);
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity("Address not updated", HttpStatus.NOT_FOUND);
        }
    }

}
