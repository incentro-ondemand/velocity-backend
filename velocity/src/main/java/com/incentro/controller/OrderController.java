package com.incentro.controller;

import com.incentro.service.OrderService;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.orders.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;
import static com.incentro.service.OrderService.getOrder;

@RestController
public class OrderController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public ResponseEntity createOrder(@RequestParam String cartId){

        try(final SphereClient client = createSphereClient()) {

            final Order order = getOrder(client, cartId);
            if (order == null) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity(order, HttpStatus.OK);
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/order/delete", method = RequestMethod.GET)
    public ResponseEntity deleteOrder(@RequestParam String orderId){
        try (final SphereClient client = createSphereClient()){
            Order order = OrderService.getOrderById(orderId).thenComposeAsync(OrderService::deleteOrder)
                    .toCompletableFuture().get();
            return new ResponseEntity(order, HttpStatus.OK);
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
}
