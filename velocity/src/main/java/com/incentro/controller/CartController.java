package com.incentro.controller;

import com.incentro.service.CartService;
import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.queries.CartByIdGet;
import io.sphere.sdk.client.SphereClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;

@RestController
public class CartController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ResponseEntity getCartById(@RequestParam String cartId) {
        try (final SphereClient client = createSphereClient()) {
            if (CartService.cartExists(client, cartId)) {
                return new ResponseEntity<>(CartService.buildCart(CartService.getCart(client, cartId).toCompletableFuture().get()), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Couldn't find cart!", HttpStatus.BAD_REQUEST);
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/cart/add", method = RequestMethod.GET)
    public ResponseEntity addProductToCart(@RequestParam String sku,
                                           @RequestParam(required = false) String cartId,
                                           @RequestParam(required = false) String customerId,
                                           @RequestParam Long quantity) {
        try (final SphereClient client = createSphereClient()) {

            if (CartService.checkAvailability(client, quantity, sku)) {
                if (cartId == null) {
                    if (customerId == null) {
                        Cart cart = CartService.createCart(client).toCompletableFuture().get();
                        return new ResponseEntity<>(CartService.buildCart(CartService.addProductToCart(client, sku, cart, quantity).toCompletableFuture().get()), HttpStatus.OK);
                    } else {
                        Cart cart = CartService.createCartWithCustomer(client, customerId).toCompletableFuture().get();
                        return new ResponseEntity<>(CartService.buildCart(CartService.addProductToCart(client, sku, cart, quantity).toCompletableFuture().get()), HttpStatus.OK);
                    }
                } else {
                    if (CartService.cartExists(client, cartId)) {
                        Cart cart = client.execute(CartByIdGet.of(cartId)).toCompletableFuture().get();
                        return new ResponseEntity<>(CartService.buildCart(CartService.addProductToCart(client, sku, cart, quantity).toCompletableFuture().get()), HttpStatus.OK);
                    } else {
                        return new ResponseEntity<>("Couldn't find cart!", HttpStatus.BAD_REQUEST);
                    }
                }
            } else {
                return new ResponseEntity<>("Quantity exceeds stock!", HttpStatus.BAD_REQUEST);
            }
        } catch (IOException | ExecutionException | InterruptedException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/cart/update", method = RequestMethod.GET)
    public ResponseEntity updateItem(@RequestParam String sku,
                                     @RequestParam String cartId,
                                     @RequestParam Long quantity) {
        try (final SphereClient client = createSphereClient()) {

            if (CartService.cartExists(client, cartId)) {
                if (CartService.checkAvailability(client, quantity, sku)) {
                    return new ResponseEntity<>(CartService.buildCart(CartService.updateLineItem(client, sku, cartId, quantity).toCompletableFuture().get()), HttpStatus.OK);
                } else {
                    return new ResponseEntity<>("Quantity exceeds stock!", HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<>("Couldn't find cart!", HttpStatus.BAD_REQUEST);
            }
        } catch (IOException | ExecutionException | InterruptedException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "cart/header", method = RequestMethod.GET)
    public ResponseEntity getHeaderData(@RequestParam String cartId) {
        try (final SphereClient client = createSphereClient()) {
            if (CartService.cartExists(client, cartId)) {
                return new ResponseEntity<>(CartService.getHeaderData(client, cartId), HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Non existent cart", HttpStatus.BAD_REQUEST);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/cart/discountcode", method = RequestMethod.GET)
    public ResponseEntity addDiscountCode(@RequestParam String discountCode,
                                          @RequestParam String cartId) {
        try (final SphereClient client = createSphereClient()) {
            final Cart cart = CartService.addDiscountCodeToCart(client, discountCode, cartId).toCompletableFuture().get();
            return new ResponseEntity<>(CartService.buildCart(cart), HttpStatus.OK);
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
        }
    }
}
