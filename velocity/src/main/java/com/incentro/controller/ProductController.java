package com.incentro.controller;

import com.incentro.model.CustomProduct;
import com.incentro.model.GraphQLProduct;
import com.incentro.service.*;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.products.ProductProjection;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;


@RestController
public class ProductController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public ResponseEntity getProduct(@RequestParam(required = false) String key) {

        try (final SphereClient client = createSphereClient()) {

            final ProductService productService = new ProductService(client);
            if (key == null) {
                return new ResponseEntity<>("Key is mandatory!", HttpStatus.BAD_REQUEST);
            }
            final ProductProjection productProjection = productService.getProductData(key);
            if (productProjection == null) {
                return new ResponseEntity<>("Product Not Found", HttpStatus.NOT_FOUND);
            }
            final CustomProduct product = productService.setProductData(productProjection);
            if (product == null) {
                return new ResponseEntity<>("Product Not Found", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(product, HttpStatus.OK);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/graphqlproduct", method = RequestMethod.GET)
    public ResponseEntity getGraphQLProduct(@RequestParam(required = false) String key) {

        try (final SphereClient client = createSphereClient()) {

            final GraphQLProductService graphQLProductService = new GraphQLProductService(client);
            if (key == null) {
                return new ResponseEntity<>("Key is mandatory!", HttpStatus.BAD_REQUEST);
            }
            final GraphQLProduct graphQLProduct = graphQLProductService.getGraphQLProductByKey(key);
            if (graphQLProduct == null) {
                return new ResponseEntity<>("Product Not Found", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(graphQLProduct, HttpStatus.OK);
            }
        } catch (InterruptedException | ExecutionException | IOException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
