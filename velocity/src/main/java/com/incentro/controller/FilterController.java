package com.incentro.controller;

import com.incentro.service.FilterService;
import com.incentro.service.QueryByCategoryService;
import io.sphere.sdk.client.SphereClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;

@RestController
public class FilterController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public ResponseEntity filterItems(@RequestParam MultiValueMap<String, String> allArgs) {
        String category = "";
        for (String key : allArgs.keySet()) {
            if (key.equals("category")) {
                category = allArgs.get(key).get(0);
            }
        }

        try (final SphereClient client = createSphereClient()) {
            final QueryByCategoryService productQueryService = new QueryByCategoryService(client);
            final FilterService filterService = new FilterService(client);
            if (filterService.filterProducts(allArgs) == null) {
                return new ResponseEntity<>("Wrong parameters", HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(productQueryService.setPagedResult(filterService.filterProducts(allArgs).toCompletableFuture().get(), category), HttpStatus.OK);
            }
        } catch (IOException | ExecutionException | InterruptedException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
