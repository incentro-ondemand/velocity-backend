package com.incentro.controller;

import com.incentro.service.QueryByCategoryService;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.queries.PagedQueryResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

import static com.incentro.service.ClientService.createSphereClient;

@RestController
public class CategoryController extends BaseController {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public ResponseEntity findByCategory(@RequestParam(required = false) String category,
                                         @RequestParam(required = false) Integer limit,
                                         @RequestParam(required = false) Integer page) {

        try (final SphereClient client = createSphereClient()) {

            if (category == null || limit == null || page == null) {
                return new ResponseEntity<>("Please specify all parameters!", HttpStatus.BAD_REQUEST);
            }
            final QueryByCategoryService productQueryService = new QueryByCategoryService(client);
            if (productQueryService.findProductsWithCategory(Locale.ENGLISH, category, limit, page) == null) {
                return new ResponseEntity<>("Category not found!", HttpStatus.BAD_REQUEST);
            } else if (productQueryService.findProductsWithCategory(Locale.ENGLISH, category, limit, page).toCompletableFuture().get().getCount() < 1) {
                return new ResponseEntity<>("Page number exceeds number of products!", HttpStatus.BAD_REQUEST);
            } else {
                final PagedQueryResult<ProductProjection> categoryQuery = productQueryService.findProductsWithCategory(Locale.ENGLISH, category, limit, page).toCompletableFuture().get();
                return new ResponseEntity<>(productQueryService.setPagedResult(categoryQuery, category), HttpStatus.OK);
            }
        } catch (IOException | InterruptedException | ExecutionException e) {
            LOGGER.severe(e.toString());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
