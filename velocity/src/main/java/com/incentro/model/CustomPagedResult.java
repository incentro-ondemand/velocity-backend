package com.incentro.model;

import io.sphere.sdk.categories.Category;

import java.util.ArrayList;
import java.util.List;

public class CustomPagedResult {
    private Category category;
    private Long offset;
    private Long limit;
    private Long total;
    private List<CustomProduct> Results = new ArrayList<>();
    private Long count;
    private Long totalPages;
    private Long pageIndex;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getLimit() {
        return limit;
    }

    public void setLimit(Long limit) {
        this.limit = limit;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<CustomProduct> getResults() {
        return Results;
    }

    public void setResults(List<CustomProduct> results) {
        Results = results;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Long totalPages) {
        this.totalPages = totalPages;
    }

    public Long getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Long pageIndex) {
        this.pageIndex = pageIndex;
    }
}