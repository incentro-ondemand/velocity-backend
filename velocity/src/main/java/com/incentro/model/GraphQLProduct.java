package com.incentro.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.sphere.sdk.categories.Category;
import io.sphere.sdk.models.LocalizedString;

import java.time.LocalDateTime;
import java.util.*;

public class GraphQLProduct {

    private String id;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
    private LocalizedString name;
    private LocalizedString description;
    private Set<Category> categories;
    private Set<CustomVariant> variants;

    public GraphQLProduct (){}

    @JsonCreator
    public static GraphQLProduct createGraphQLProduct(@JsonProperty("id")  String id,
                                                @JsonProperty("createdAt") LocalDateTime createdAt,
                                                @JsonProperty("lastModifiedAt") LocalDateTime lastModifiedAt,
                                                @JsonProperty("masterData") Map<?,?> masterData
    ) {
        final Locale eng = Locale.ENGLISH;
        final Map<?,?> current = (Map<?,?>) masterData.get("current");
        final GraphQLProduct graphQLProduct = new GraphQLProduct();

        graphQLProduct.id = id;
        graphQLProduct.createdAt = createdAt;
        graphQLProduct.lastModifiedAt = lastModifiedAt;
        graphQLProduct.name = ((LocalizedString.of(eng, (String) current.get("name"))));
        if ((String) current.get("description") != null) {
            graphQLProduct.description = ((LocalizedString.of(eng, (String)current.get("description"))));
        } else {
            graphQLProduct.description = ((LocalizedString.of(eng, "This product has no description yet")));
        }
        graphQLProduct.categories = new HashSet<Category>((List)current.get("categories"));
        graphQLProduct.variants = new HashSet<CustomVariant>((List)current.get("allVariants"));

        return graphQLProduct;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(LocalDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public LocalizedString getName() {
        return name;
    }

    public void setName(LocalizedString name) {
        this.name = name;
    }

    public LocalizedString getDescription() {
        return description;
    }

    public void setDescription(LocalizedString description) {
        this.description = description;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public Set<CustomVariant> getVariants() {
        return variants;
    }

    public void setVariants(Set<CustomVariant> variants) {
        this.variants = variants;
    }

}
