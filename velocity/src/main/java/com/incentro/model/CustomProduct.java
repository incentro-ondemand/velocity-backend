package com.incentro.model;

import io.sphere.sdk.categories.Category;
import io.sphere.sdk.models.LocalizedString;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CustomProduct {
    private String id;
    private String key;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
    private LocalizedString name;
    private LocalizedString description;
    private Set<Category> categories;
    private List<CustomVariant> variants = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(LocalDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public LocalizedString getName() {
        return name;
    }

    public void setName(LocalizedString name) {
        this.name = name;
    }

    public LocalizedString getDescription() {
        return description;
    }

    public void setDescription(LocalizedString description) {
        this.description = description;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }

    public List<CustomVariant> getVariants() {
        return variants;
    }

    public void setVariants(List<CustomVariant> variants) {
        this.variants = variants;
    }
}
