package com.incentro.model;

import io.sphere.sdk.customers.CustomerName;

import javax.money.NumberValue;

public class SimplifiedCartModel {
    private CustomerName customerName;
    private NumberValue totalPrice;
    private Long totalObjects;

    public CustomerName getCustomerName() {
        return customerName;
    }

    public void setCustomerName(CustomerName customerName) {
        this.customerName = customerName;
    }

    public NumberValue getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(NumberValue totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getTotalObjects() {
        return totalObjects;
    }

    public void setTotalObjects(Long totalObjects) {
        this.totalObjects = totalObjects;
    }
}
