package com.incentro.model;

import io.sphere.sdk.carts.CartShippingInfo;
import io.sphere.sdk.carts.LineItem;
import io.sphere.sdk.carts.PaymentInfo;
import io.sphere.sdk.models.Address;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomCart {
    private String id;
    private Address shippingAddress;
    private Address billingAddress;
    private CartShippingInfo shippingInfo;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
    private String customerId;
    private List<CustomDiscountCode> discountCodes = new ArrayList<>();
    private PaymentInfo paymentInfo;
    private CustomPrice totalPrice;
    private Long version;
    private List<LineItem> lineItems = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public CartShippingInfo getShippingInfo() {
        return shippingInfo;
    }

    public void setShippingInfo(CartShippingInfo shippingInfo) {
        this.shippingInfo = shippingInfo;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(LocalDateTime lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public List<CustomDiscountCode> getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(List<CustomDiscountCode> discountCodes) {
        this.discountCodes = discountCodes;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public CustomPrice getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(CustomPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }
}
