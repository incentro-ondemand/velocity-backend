package com.incentro.model;

import io.sphere.sdk.cartdiscounts.CartDiscount;
import io.sphere.sdk.models.LocalizedString;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomDiscountCode {
    private List<CartDiscount>  cartDiscounts = new ArrayList<>();
    private String code;
    private LocalizedString description;
    private String id;
    private ZonedDateTime validFrom;
    private ZonedDateTime validUntil;

    public List<CartDiscount> getCartDiscounts() {
        return cartDiscounts;
    }

    public void setCartDiscounts(List<CartDiscount> cartDiscounts) {
        this.cartDiscounts = cartDiscounts;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalizedString getDescription() {
        return description;
    }

    public void setDescription(LocalizedString description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZonedDateTime getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(ZonedDateTime validFrom) {
        this.validFrom = validFrom;
    }

    public ZonedDateTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(ZonedDateTime validUntil) {
        this.validUntil = validUntil;
    }
}
