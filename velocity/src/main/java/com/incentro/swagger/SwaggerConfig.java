package com.incentro.swagger;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@PropertySource("classpath:application.properties")
@EnableSwagger2
@Configuration
@EnableWebMvc
public class SwaggerConfig extends WebMvcConfigurerAdapter {

    @Value("${http.auth.tokenName}")
    private String authHeaderName;

    @Value("${http.auth.tokenValue}")
    private String authHeaderValue;

    @Bean
    public Docket swagger() {
        return
                new Docket(DocumentationType.SWAGGER_2)
                        .apiInfo(apiInfo())
                        .securityContexts(Lists.newArrayList(securityContext()))
                        .securitySchemes(Lists.newArrayList(apiKey()))
                        .select()
                        .apis(RequestHandlerSelectors.basePackage("com.incentro.controller"))
                        .paths(regex("/api.*"))
                        .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("API Key", authHeaderName, authHeaderValue);
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope
                = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference("API Key", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Velocity REST API",
                "Velocity backend",
                "0.1",
                "Terms of service",
                new Contact("Nathan Ernst", "incentro.com", "nathan.ernst@incentro.com "),
                "License of API", "API license URL", Collections.emptyList());
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        //enabling swagger-ui part for visual documentation
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

}
