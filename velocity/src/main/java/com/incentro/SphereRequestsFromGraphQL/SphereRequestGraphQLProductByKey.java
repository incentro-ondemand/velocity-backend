package com.incentro.SphereRequestsFromGraphQL;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.incentro.model.GraphQLProduct;
import io.sphere.sdk.client.HttpRequestIntent;
import io.sphere.sdk.client.SphereRequest;
import io.sphere.sdk.http.HttpMethod;
import io.sphere.sdk.http.HttpResponse;
import io.sphere.sdk.json.SphereJsonUtils;
import io.sphere.sdk.models.Base;
import org.apache.commons.lang3.Validate;

import javax.annotation.Nullable;

public class SphereRequestGraphQLProductByKey extends Base implements SphereRequest<GraphQLProduct> {

    final private String key;

    public SphereRequestGraphQLProductByKey(String key){
        Validate.notEmpty(key);
        this.key = key;
    }


    @Nullable
    @Override
    public GraphQLProduct deserialize(HttpResponse httpResponse) {

        final JsonNode productResult = SphereJsonUtils.parse(httpResponse.getResponseBody())
                .get("data").get("product");

        return SphereJsonUtils.readObject(productResult, new TypeReference<GraphQLProduct>() { });
    }

    @Override
    public HttpRequestIntent httpRequestIntent() {
        final String queryString = String.format("query productByKey($key: String!) {\n" +
                "       product(key: $key) {\n" +
                "               id\n" +
                "               version\n" +
                "               createdAt\n" +
                "               lastModifiedAt\n" +
                "               key\n" +
                "               masterData {\n" +
                "                   current {\n" +
                "                       name(locale: \"en\") \n   " +
                "                       description(locale: \"en\") \n   " +
                "                       categories{\n " +
                "                           id \n " +
                "                       }\n    " +
                "                       allVariants{\n " +
                "                           id \n " +
                "                       }\n    " +
                "                   }\n" +
                "               }\n" +
                "       }\n" +
                "   }");
        final String body = String.format(
                "{\n" +
                        "   \"query\": \"%s\", \n" +
                        "   \"variables\": { \"key\": \"%s\" }\n" +
                        "}",
                queryString.replace("\n", "\\n")
                        .replace("\"", "\\\""),
                key);
        return HttpRequestIntent.of(HttpMethod.POST, "/graphql", body);
    }
}
