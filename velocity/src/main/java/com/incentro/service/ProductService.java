package com.incentro.service;

import com.incentro.model.CustomProduct;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.models.Reference;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.products.ProductProjectionType;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.expansion.ProductProjectionExpansionModel;
import io.sphere.sdk.products.queries.ProductProjectionByKeyGet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class ProductService {

    private final SphereClient client;

    public ProductService(SphereClient client) {
        this.client = client;
    }

    public io.sphere.sdk.products.ProductProjection getProductData(String key) throws ExecutionException, InterruptedException {
        return client.execute(
                ProductProjectionByKeyGet.of(key, ProductProjectionType.CURRENT)
                        .withExpansionPaths(ProductProjectionExpansionModel::categories))
                .toCompletableFuture().get();
    }

    public CustomProduct setProductData(ProductProjection product) {

        if (product == null) {
            return null;
        }
        CustomProduct populatedProduct = new CustomProduct();
        populatedProduct.setId(product.getId());
        populatedProduct.setKey(product.getKey());
        populatedProduct.setCreatedAt(product.getCreatedAt().toLocalDateTime());
        populatedProduct.setLastModifiedAt(product.getLastModifiedAt().toLocalDateTime());
        populatedProduct.setName(product.getName());
        populatedProduct.setDescription(product.getDescription());
        populatedProduct.setCategories(
                product.getCategories()
                        .stream().filter(ref -> ref.getObj() != null)
                        .map(Reference::getObj).collect(Collectors.toSet()));
        List<ProductVariant> allVariants = product.getAllVariants();

        for (ProductVariant variant:allVariants){
            populatedProduct.getVariants().add(VariantService.buildVariant(variant));
        }

        return populatedProduct;
    }
}
