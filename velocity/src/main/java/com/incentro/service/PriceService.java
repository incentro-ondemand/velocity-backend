package com.incentro.service;

import com.incentro.model.CustomPrice;
import io.sphere.sdk.products.Price;

import javax.money.MonetaryAmount;

public class PriceService {
    public CustomPrice buildPrice(MonetaryAmount price){
        CustomPrice customPrice = new CustomPrice();
        customPrice.setCurrency(price.getCurrency().toString());
        customPrice.setValue(price.getNumber().toString());
        return customPrice;
    }
}
