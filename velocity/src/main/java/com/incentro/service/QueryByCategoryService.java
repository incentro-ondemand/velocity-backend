package com.incentro.service;

import com.incentro.model.CustomPagedResult;
import io.sphere.sdk.categories.Category;
import io.sphere.sdk.categories.queries.CategoryQuery;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.products.expansion.ProductProjectionExpansionModel;
import io.sphere.sdk.products.queries.ProductProjectionQuery;
import io.sphere.sdk.queries.PagedQueryResult;
import io.sphere.sdk.queries.PagedResult;

import java.util.Collections;
import java.util.Locale;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class QueryByCategoryService {
    private final SphereClient client;

    public QueryByCategoryService(SphereClient client) {
        this.client = client;
    }

    private CompletionStage<PagedQueryResult<Category>> findCategory(final Locale locale, final String name) {
        return client.execute(CategoryQuery.of().byName(locale, name));
    }

    private CompletionStage<PagedQueryResult<ProductProjection>> withCategory(final Category category, final int limit, final int page) {
        final ProductProjectionQuery query = ProductProjectionQuery.ofStaged()
                .withPredicates(m -> m.categories().isIn(Collections.singletonList(category)))
                .withSort(m -> m.name().lang(Locale.ENGLISH).sort().asc())
                .withLimit(limit)
                .withOffset(page * limit)
                .withExpansionPaths(ProductProjectionExpansionModel::categories);
        return client.execute(query);
    }

    public CompletionStage<PagedQueryResult<ProductProjection>> findProductsWithCategory(final Locale locale, final String name, final int limit, final int page) throws ExecutionException, InterruptedException {
        if (findCategory(locale,name) == null){
            return null;
        }
        final PagedQueryResult<Category> categoryPagedQueryResult = findCategory(locale, name).toCompletableFuture().get();
        for (Category category:categoryPagedQueryResult.getResults()){
            return withCategory(category, limit, page);
        }
        return null;
    }

    public CustomPagedResult setPagedResult(PagedResult<ProductProjection> categoryQuery, String category) throws ExecutionException, InterruptedException {

        final ProductService productService = new ProductService(client);
        CustomPagedResult pagedResult = new CustomPagedResult();
        if(category != null && !category.isEmpty()){
            pagedResult.setCategory(findCategory(Locale.ENGLISH, category).toCompletableFuture().get().getResults().get(0));
        }
        pagedResult.setOffset(categoryQuery.getOffset());
        pagedResult.setLimit(categoryQuery.getLimit());
        pagedResult.setTotal(categoryQuery.getTotal());
        pagedResult.setCount(categoryQuery.getCount());
        pagedResult.setTotalPages(categoryQuery.getTotalPages());
        pagedResult.setPageIndex(categoryQuery.getPageIndex());
        for (ProductProjection product:categoryQuery.getResults()) {
            pagedResult.getResults().add(productService.setProductData(product));
        }
        return pagedResult;
    }
}