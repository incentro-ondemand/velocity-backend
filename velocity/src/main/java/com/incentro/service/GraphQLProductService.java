package com.incentro.service;

import com.incentro.SphereRequestsFromGraphQL.SphereRequestGraphQLProductByKey;
import com.incentro.model.GraphQLProduct;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.client.SphereRequest;

import java.util.concurrent.ExecutionException;

public class GraphQLProductService {

    final private SphereClient client;

    public GraphQLProductService(SphereClient client){
        this.client = client;
    }

    public GraphQLProduct getGraphQLProductByKey(String key) throws InterruptedException, ExecutionException {
        SphereRequest<GraphQLProduct> SphereRequestGraphQLProduct = new SphereRequestGraphQLProductByKey(key);

        return client.execute(SphereRequestGraphQLProduct).toCompletableFuture().get();
    }

}
