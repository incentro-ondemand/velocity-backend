package com.incentro.service;

import com.incentro.model.CustomCart;
import com.incentro.model.SimplifiedCartModel;
import io.sphere.sdk.carts.*;
import io.sphere.sdk.carts.commands.CartCreateCommand;
import io.sphere.sdk.carts.commands.CartUpdateCommand;
import io.sphere.sdk.carts.commands.updateactions.AddDiscountCode;
import io.sphere.sdk.carts.commands.updateactions.AddLineItem;
import io.sphere.sdk.carts.commands.updateactions.SetShippingAddress;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.customers.Customer;
import io.sphere.sdk.discountcodes.DiscountCodeInfo;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.models.DefaultCurrencyUnits;
import io.sphere.sdk.products.ProductProjection;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.LineItem;
import io.sphere.sdk.carts.commands.updateactions.ChangeLineItemQuantity;
import io.sphere.sdk.carts.queries.CartByIdGet;
import io.sphere.sdk.products.search.ProductProjectionSearch;

import java.util.Objects;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class CartService {

    public static CompletionStage<Cart> getCart(SphereClient client, final String id) {
        return client.execute(CartByIdGet.of(id)
                .withExpansionPaths(m -> m.discountCodes().discountCode())
                .plusExpansionPaths(m -> m.discountCodes().discountCode().cartDiscounts()));
    }

    public static Boolean cartExists(SphereClient client, final String cartId) throws ExecutionException, InterruptedException {
        return client.execute(CartByIdGet.of(cartId)).toCompletableFuture().get() != null;
    }

    public static CompletionStage<Cart> createCart(SphereClient client) {

        final CartDraft cartDraft = CartDraftBuilder.of(DefaultCurrencyUnits.EUR)
                .deleteDaysAfterLastModification(1)
                .build();

        final CartCreateCommand cartCreateCommand = CartCreateCommand.of(cartDraft);
        return client.execute(cartCreateCommand);
    }

    public static CompletionStage<Cart> createCartWithCustomer(SphereClient client, String customerId) throws ExecutionException, InterruptedException {

        Address shippingAddress = null;
        Address customerShippingAddress = CustomerService.getCustomer(client, customerId).toCompletableFuture().get().getDefaultShippingAddress();

        if(customerShippingAddress!=null){
            shippingAddress = customerShippingAddress;
        }

        final CartDraft cartDraft = CartDraftBuilder.of(DefaultCurrencyUnits.EUR)
                .deleteDaysAfterLastModification(1)
                .customerId(customerId)
                .shippingAddress(shippingAddress)
                .build();

        final CartCreateCommand cartCreateCommand = CartCreateCommand.of(cartDraft)
                .withExpansionPaths(m -> m.discountCodes().discountCode())
                .plusExpansionPaths(m -> m.discountCodes().discountCode().cartDiscounts());
        return client.execute(cartCreateCommand);
    }

    public static CompletionStage<Cart> addProductToCart(SphereClient client, String sku, Cart cart, Long quantity) {

        final LineItemDraft lineItemDraft =
                LineItemDraftBuilder.ofSku(sku, quantity)
                        .build();
        final AddLineItem addLineItem = AddLineItem.of(lineItemDraft);
        final CartUpdateCommand cartUpdateCommand = CartUpdateCommand.of(cart, addLineItem)
                .withExpansionPaths(m -> m.discountCodes().discountCode())
                .plusExpansionPaths(m -> m.discountCodes().discountCode().cartDiscounts());
        return client.execute(cartUpdateCommand);
    }

    public static CompletionStage<Cart> updateLineItem(SphereClient client, String sku, String id, Long quantity) throws ExecutionException, InterruptedException {

        final Cart cart = getCart(client, id).toCompletableFuture().get();
        for (LineItem lineItem : cart.getLineItems()) {
            if (Objects.equals(lineItem.getVariant().getSku(), sku)) {
                return client.execute(CartUpdateCommand.of(cart, ChangeLineItemQuantity.of(lineItem, quantity))
                        .withExpansionPaths(m -> m.discountCodes().discountCode())
                        .plusExpansionPaths(m -> m.discountCodes().discountCode().cartDiscounts()));
            }
        }
        return null;
    }

    public static boolean checkAvailability(SphereClient client, Long quantity, String sku) throws ExecutionException, InterruptedException {

        ProductProjection productProjection = client.execute(ProductProjectionSearch.ofCurrent().bySku(sku)).toCompletableFuture().get().getResults().get(0);
        if (productProjection.findVariantBySku(sku).isPresent()) {
            if (productProjection.findVariantBySku(sku).get().getAvailability() != null) {
                Long quantityOnStock = Objects.requireNonNull(productProjection.findVariantBySku(sku).get().getAvailability()).getAvailableQuantity();
                return (quantityOnStock != null) && (quantityOnStock >= quantity);
            }
        }
        return false;
    }

    public static CompletionStage<Cart> addDiscountCodeToCart(SphereClient client, String discountCode, String cartId) throws ExecutionException, InterruptedException {

        if (cartExists(client, cartId)) {
            final Cart cart = getCart(client, cartId).toCompletableFuture().get();
            final AddDiscountCode addDiscountCode = AddDiscountCode.of(discountCode);
            final CartUpdateCommand cartUpdateCommand = CartUpdateCommand.of(cart, addDiscountCode)
                    .withExpansionPaths(m -> m.discountCodes().discountCode())
                    .plusExpansionPaths(m -> m.discountCodes().discountCode().cartDiscounts());
            return client.execute(cartUpdateCommand);
        } else {
            return null;
        }
    }

    public static SimplifiedCartModel getHeaderData(SphereClient client, String cartId) throws ExecutionException, InterruptedException {

        final Cart cart = getCart(client, cartId).toCompletableFuture().get();
        SimplifiedCartModel simplifiedCartModel = new SimplifiedCartModel();
        simplifiedCartModel.setTotalPrice(cart.getTotalPrice().getNumber());
        simplifiedCartModel.setTotalObjects(0L);
        for (LineItem lineItem : cart.getLineItems()) {
            simplifiedCartModel.setTotalObjects(simplifiedCartModel.getTotalObjects() + lineItem.getQuantity());
        }
        if (cart.getCustomerId() != null) {
            final Customer customer = CustomerService.getCustomer(client, cart.getCustomerId()).toCompletableFuture().get();
            simplifiedCartModel.setCustomerName(customer.getName());
        }

        return simplifiedCartModel;
    }

    public static CustomCart buildCart(final Cart cart) {
        PriceService priceService = new PriceService();
        DiscountCodeService discountCodeService = new DiscountCodeService();
        CustomCart customCart = new CustomCart();
        customCart.setId(cart.getId());
        customCart.setShippingAddress(cart.getShippingAddress());
        customCart.setBillingAddress(cart.getBillingAddress());
        customCart.setShippingInfo(cart.getShippingInfo());
        customCart.setCreatedAt(cart.getCreatedAt().toLocalDateTime());
        customCart.setLastModifiedAt(cart.getLastModifiedAt().toLocalDateTime());
        customCart.setCustomerId(cart.getCustomerId());
        customCart.setPaymentInfo(cart.getPaymentInfo());
        customCart.setVersion(cart.getVersion());

        for (DiscountCodeInfo codeInfo : cart.getDiscountCodes()) {
            customCart.getDiscountCodes().add(discountCodeService.buildDiscountCode(codeInfo.getDiscountCode().getObj()));
        }
        customCart.setLineItems(cart.getLineItems());
        customCart.setTotalPrice(priceService.buildPrice(cart.getTotalPrice()));
        return customCart;
    }

    public static CompletionStage<Cart> setShippingAddress(SphereClient client, Cart cart) throws ExecutionException, InterruptedException {
        final Address shippingAddress = CustomerService.getCustomer(client, cart.getCustomerId()).toCompletableFuture().get().getDefaultShippingAddress();
        return client.execute(CartUpdateCommand.of(cart, SetShippingAddress.of(shippingAddress)));
    }
}






