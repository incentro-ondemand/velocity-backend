package com.incentro.service;

import com.incentro.model.CustomDiscountCode;
import io.sphere.sdk.cartdiscounts.CartDiscount;
import io.sphere.sdk.discountcodes.DiscountCode;
import io.sphere.sdk.models.Reference;

public class DiscountCodeService {
    public CustomDiscountCode buildDiscountCode(DiscountCode discountCode){
        CustomDiscountCode customDiscountCode = new CustomDiscountCode();
        for (Reference<CartDiscount> cartDiscount:discountCode.getCartDiscounts()){
            customDiscountCode.getCartDiscounts().add(cartDiscount.getObj());
        }
        customDiscountCode.setCode(discountCode.getCode());
        customDiscountCode.setDescription(discountCode.getDescription());
        customDiscountCode.setId(discountCode.getId());
        customDiscountCode.setValidFrom(discountCode.getValidFrom());
        customDiscountCode.setValidUntil(discountCode.getValidUntil());

        return customDiscountCode;
    }
}
