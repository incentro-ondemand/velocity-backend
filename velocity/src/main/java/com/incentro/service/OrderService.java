package com.incentro.service;

import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.orders.Order;
import io.sphere.sdk.orders.commands.OrderDeleteCommand;
import io.sphere.sdk.orders.commands.OrderFromCartCreateCommand;
import io.sphere.sdk.orders.queries.OrderByIdGet;
import org.omg.CORBA.ORB;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class OrderService {

    private static SphereClient client;

    public OrderService(SphereClient newClient) {
        client = newClient;
    }

    public static CompletionStage<Order> createOrder(final Cart cart){
        return client.execute(OrderFromCartCreateCommand.of(cart));
    }

    public static CompletionStage<Order> getOrderById(final String orderId){
        return client.execute(OrderByIdGet.of(orderId));
    }

    public static CompletionStage<Order> deleteOrder(final Order order){
        return client.execute(OrderDeleteCommand.of(order));
    }

    public static Order getOrder(SphereClient client, String cartId) throws InterruptedException, ExecutionException {
        Cart cart = CartService.getCart(client, cartId).toCompletableFuture().get();
        if(cart.getShippingAddress()==null){
            cart = CartService.setShippingAddress(client, cart).toCompletableFuture().get();
        }
        return createOrder(cart).toCompletableFuture().get();
    }

}
