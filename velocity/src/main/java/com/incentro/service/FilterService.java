package com.incentro.service;

import io.sphere.sdk.categories.queries.CategoryQuery;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.products.ProductProjection;
import io.sphere.sdk.products.expansion.ProductProjectionExpansionModel;
import io.sphere.sdk.products.search.*;
import io.sphere.sdk.producttypes.queries.ProductTypeByKeyGet;
import io.sphere.sdk.search.FilterExpression;
import io.sphere.sdk.search.PagedSearchResult;
import io.sphere.sdk.search.SortExpression;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class FilterService {

    private final SphereClient client;

    public FilterService(SphereClient client) {
        this.client = client;
    }


    public CompletionStage<PagedSearchResult<ProductProjection>> filterProducts(MultiValueMap<String, String> map) throws ExecutionException, InterruptedException {

        List<FilterExpression<ProductProjection>> filterList = new ArrayList<>();
        List<SortExpression<ProductProjection>> sortList = new ArrayList<>();
        int limit = 20;
        int page = 0;

        for (String key:map.keySet()){
            switch (key) {
                case "limit":
                    limit = Integer.parseInt(map.get(key).get(0));
                    break;
                case "page":
                    page = Integer.parseInt(map.get(key).get(0));
                    break;
                case "sort":
                    sortList.add(buildSortExpression(map.get(key).get(0)));
                    break;
                case "price-range":
                    filterList.add(buildRangeExpression(map.get(key).get(0)));
                    break;
                case "category":
                    filterList.add(buildCategoryFilterExpression(map.get(key).get(0)));
                    break;
                default:
                    String expr = String.join("\",\"", map.get(key));
                    filterList.add(buildVariantFilterExpression(key, expr));
                    break;
            }
        }
        return executeSearch(limit, page, filterList, sortList);
    }

    private SortExpression<ProductProjection> buildSortExpression(String str) {
        String[] sortedString = str.split("-", 2);
        return SortExpression.of(sortedString[0] + " " + sortedString[1]);
    }

    private FilterExpression<ProductProjection> buildRangeExpression(String str) {
        String[] sortedString = str.split("-", 2);
        return FilterExpression.of("variants.price.centAmount:range("+sortedString[0]+" to "+sortedString[1]+")");
    }

    private FilterExpression<ProductProjection> buildCategoryFilterExpression(String category) throws ExecutionException, InterruptedException {
        if(getCategoryId(category) == null){
            return null;
        }
        else{
            return FilterExpression.of("categories.id:\"" + getCategoryId(category) + "\"");
        }
    }

    private FilterExpression<ProductProjection> buildVariantFilterExpression(String key, String values) throws ExecutionException, InterruptedException {

        if (attributeExists(key)) {
            if (attributeIsEnum(key)) {
                return FilterExpression.of("variants.attributes." + key + ".label:\"" + values + "\"");
            } else {
                return FilterExpression.of("variants.attributes." + key + ":\"" + values + "\"");
            }
        } else {
            return null;
        }
    }


    private CompletionStage<PagedSearchResult<ProductProjection>> executeSearch(final int limit, final int page, final List<FilterExpression<ProductProjection>> filterList, List<SortExpression<ProductProjection>> sortList){
        return
                client.execute(
                        ProductProjectionSearch.ofCurrent()
                                .withLimit(limit)
                                .withOffset(page*limit)
                                .plusQueryFilters(filterList)
                                .plusSort(sortList)
                                .withExpansionPaths(ProductProjectionExpansionModel::categories)
                );
    }

    private Boolean attributeExists(String attr) throws ExecutionException, InterruptedException {
        return client.execute(ProductTypeByKeyGet.of("bike")).toCompletableFuture().get().findAttribute(attr).isPresent();
    }

    private Boolean attributeIsEnum(String attr) throws ExecutionException, InterruptedException {
        return client.execute(ProductTypeByKeyGet.of("bike")).toCompletableFuture().get().getAttribute(attr).getAttributeType().getClass().getName().contains("Enum");
    }

    private String getCategoryId(String category) throws ExecutionException, InterruptedException {
        if (client.execute(CategoryQuery.of().byName(Locale.ENGLISH, category)).toCompletableFuture().get().getResults().size() == 0){
            return null;
        }
        else{
            return client.execute(CategoryQuery.of().byName(Locale.ENGLISH, category)).toCompletableFuture().get().getResults().get(0).getId();
        }
    }
}