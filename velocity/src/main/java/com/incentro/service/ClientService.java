package com.incentro.service;

import io.sphere.sdk.client.*;
import io.sphere.sdk.http.HttpClient;

import java.io.IOException;
import java.util.Properties;

public class ClientService {

    public static SphereClient createSphereClient() throws IOException {

        final SphereClientConfig clientConfig = loadCTPClientConfig();
        final HttpClient httpClient = new SphereAsyncHttpClientFactory().getClient();
        final SphereAccessTokenSupplier sphereAccessTokenSupplier =
                SphereAccessTokenSupplier.ofAutoRefresh(clientConfig, httpClient, true);
        return SphereClient.of(clientConfig, httpClient, sphereAccessTokenSupplier);

    }

    private static SphereClientConfig loadCTPClientConfig() throws IOException {

        Properties properties = new Properties();
        properties.load(SphereClientConfig.class.getResourceAsStream("/dev.properties"));
        return SphereClientConfig.ofProperties(properties, "ctp.");

    }
}
