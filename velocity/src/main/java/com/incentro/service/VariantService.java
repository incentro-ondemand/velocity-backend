package com.incentro.service;

import com.incentro.model.CustomVariant;
import io.sphere.sdk.products.ProductVariant;
import io.sphere.sdk.products.attributes.Attribute;

import java.util.HashSet;
import java.util.Set;

public class VariantService {

    public static CustomVariant buildVariant(ProductVariant variant){

        CustomVariant sortedVariant = new CustomVariant();
        Set<Attribute> frameAttributes = new HashSet<>();
        Set<Attribute> saddleAttributes = new HashSet<>();
        Set<Attribute> batteryAttributes = new HashSet<>();
        Set<Attribute> tyreAttributes = new HashSet<>();
        Set<Attribute> generalAttributes = new HashSet<>();

        sortedVariant.setSku(variant.getSku());
        sortedVariant.setId(variant.getId());
        sortedVariant.setPrices(variant.getPrices());
        sortedVariant.setImages(variant.getImages());
        sortedVariant.setAvailability(variant.getAvailability());

        for(Attribute attribute:variant.getAttributes()){

            if (attribute.getName().contains("frame")){
                frameAttributes.add(attribute);
            }
            else if (attribute.getName().contains("saddle")){
                saddleAttributes.add(attribute);
            }
            else if (attribute.getName().contains("wheel") || attribute.getName().contains("tyre")  ){
                tyreAttributes.add(attribute);
            }
            else if (attribute.getName().contains("action") || attribute.getName().contains("battery")  ){
                batteryAttributes.add(attribute);
            }
            else{
                generalAttributes.add(attribute);
            }
        }

        sortedVariant.getAllAttributes().put("Tyres",tyreAttributes);
        sortedVariant.getAllAttributes().put("Frame",frameAttributes);
        sortedVariant.getAllAttributes().put("Saddle",saddleAttributes);
        sortedVariant.getAllAttributes().put("Battery",batteryAttributes);
        sortedVariant.getAllAttributes().put("General",generalAttributes);

        return sortedVariant;

    }
}
