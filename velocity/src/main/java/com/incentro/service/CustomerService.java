package com.incentro.service;

import com.neovisionaries.i18n.CountryCode;
import io.sphere.sdk.carts.Cart;
import io.sphere.sdk.carts.queries.CartByCustomerIdGet;
import io.sphere.sdk.client.SphereClient;
import io.sphere.sdk.commands.UpdateAction;
import io.sphere.sdk.customers.*;
import io.sphere.sdk.customers.commands.*;
import io.sphere.sdk.customers.commands.updateactions.*;
import io.sphere.sdk.customers.queries.CustomerByIdGet;
import io.sphere.sdk.models.Address;
import io.sphere.sdk.models.AddressBuilder;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Nullable;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static java.util.Arrays.asList;

public class CustomerService {

    public static CompletionStage<CustomerSignInResult> createCustomer(SphereClient client, String firstName, String lastName,
                                                                       String email, String password) {

        CustomerName customerName = CustomerName.ofFirstAndLastName(firstName, lastName);
        final CustomerDraft customerDraft =
                CustomerDraftBuilder.of(customerName, email, password)
                        .build();
        final CustomerCreateCommand customerCreateCommand = CustomerCreateCommand.of(customerDraft);
        return client.execute(customerCreateCommand);
    }

    public static CompletionStage<CustomerToken> createEmailVerificationToken(SphereClient client, Customer customer, Integer timeToLiveInMinutes) {

        final CustomerCreateEmailTokenCommand customerCreateEmailTokenCommand = CustomerCreateEmailTokenCommand.of(customer, timeToLiveInMinutes);
        return client.execute(customerCreateEmailTokenCommand);
    }

    public static CompletionStage<Customer> verifyEmail(SphereClient client, CustomerToken customerToken) {

        final CustomerVerifyEmailCommand customerVerifyEmailCommand = CustomerVerifyEmailCommand.ofCustomerToken(customerToken);
        return client.execute(customerVerifyEmailCommand);
    }

    public static CompletionStage<Customer> getCustomer(SphereClient client, String customerId) {

        return client.execute(CustomerByIdGet.of(customerId));
    }

    public static CompletionStage<Cart> getCartByCustomer(SphereClient client, String customerId) {

        return client.execute(CartByCustomerIdGet.of(customerId));
    }

    public static CompletionStage<CustomerSignInResult> loginCustomer(SphereClient client, String customerEmail, String customerPassword) {

        final CustomerSignInCommand customerSignInCommand = CustomerSignInCommand.of(customerEmail, customerPassword);
        return client.execute(customerSignInCommand);
    }

    public static CompletionStage<CustomerSignInResult> loginCustomerWithAnonymousCart(SphereClient client, String customerEmail,
                                                                                       String customerPassword, String anonymousCartId) {

        final CustomerSignInCommand customerSignInCommand = CustomerSignInCommand.of(customerEmail, customerPassword, anonymousCartId);
        return client.execute(customerSignInCommand);
    }

    public static CompletionStage<Customer> addAddress(SphereClient client, Customer customer, @Nullable String countryCode,
                                                       @Nullable String city, @Nullable String region,
                                                       @Nullable String state, @Nullable String streetName,
                                                       @Nullable String streetNumber, @Nullable String postalCode,
                                                       @Nullable String firstName, @Nullable String lastName,
                                                       @Nullable String salutation, @Nullable String title,
                                                       @Nullable String company, @Nullable String department,
                                                       @Nullable String email, @Nullable String phone,
                                                       @Nullable String mobile, @Nullable String fax) {
        final CountryCode newCountryCode = CountryCode.valueOf(countryCode);
        final Address address = AddressBuilder.of(newCountryCode)
                .city(city)
                .region(region)
                .state(state)
                .streetName(streetName)
                .streetNumber(streetNumber)
                .postalCode(postalCode)
                .firstName(firstName)
                .lastName(lastName)
                .salutation(salutation)
                .title(title)
                .company(company)
                .department(department)
                .email(email)
                .phone(phone)
                .mobile(mobile)
                .fax(fax)
                .build();
        return client.execute(CustomerUpdateCommand.of(customer, AddAddress.of(address)));
    }

    public static CompletionStage<Customer> updateAddress(SphereClient client, Customer customer,
                                                          Address oldAddress, @Nullable String countryCode,
                                                          @Nullable String city, @Nullable String region,
                                                          @Nullable String state, @Nullable String streetName,
                                                          @Nullable String streetNumber, @Nullable String postalCode,
                                                          @Nullable String firstName, @Nullable String lastName,
                                                          @Nullable String salutation, @Nullable String title,
                                                          @Nullable String company, @Nullable String department,
                                                          @Nullable String email, @Nullable String phone,
                                                          @Nullable String mobile, @Nullable String fax) {
        final CountryCode newCountryCode = CountryCode.valueOf(countryCode);
        final Address newAddress = oldAddress
                .withCountry(newCountryCode)
                .withCity(city)
                .withRegion(region)
                .withState(state)
                .withStreetName(streetName)
                .withStreetNumber(streetNumber)
                .withPostalCode(postalCode)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withSalutation(salutation)
                .withTitle(title)
                .withCompany(company)
                .withDepartment(department)
                .withEmail(email)
                .withPhone(phone)
                .withMobile(mobile)
                .withFax(fax);
        final ChangeAddress updateAction = ChangeAddress.ofOldAddressToNewAddress(oldAddress, newAddress);
        return client.execute(CustomerUpdateCommand.of(customer, updateAction));
    }

    public static CompletionStage<Customer> changeName(SphereClient client, Customer customer, @Nullable String title,
                                                       @Nullable String firstName, @Nullable String lastName){
        final List<UpdateAction<Customer>> updateActions = asList(
                SetTitle.of(title),
                SetFirstName.of(firstName),
                SetLastName.of(lastName)
        );
        return client.execute(CustomerUpdateCommand.of(customer, updateActions));
    }

    public static CompletionStage<Customer> setDefaultShippingAddress(SphereClient client, Customer customer){
        Address address = customer.getAddresses().get(0);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultShippingAddress.ofAddress(address)));
    }

    public static CompletionStage<Customer> setDefaultShippingAddress (SphereClient client, Customer customer, String postalCode){
        final String id = customer.getAddresses().stream().filter(a -> a.getPostalCode().equals(postalCode)).findFirst().get().getId();
        final Address address = customer.getAddressById(id);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultShippingAddress.ofAddress(address)));
    }

    public static CompletionStage<Customer> setDefaultShippingAddressWithAddressId(SphereClient client, Customer customer,
                                                                                   String addressId) {
        final Address address = customer.getAddressById(addressId);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultShippingAddress.ofAddress(address)));
    }

    public static CompletionStage<Customer> setDefaultSBillingAddress(SphereClient client, Customer customer){
        Address address = customer.getAddresses().get(0);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultBillingAddress.ofAddress(address)));
    }

    public static CompletionStage<Customer> setDefaultBillingAddress(SphereClient client, Customer customer, String postalCode){
        final String id = customer.getAddresses().stream().filter(a -> a.getPostalCode().equals(postalCode)).findFirst().get().getId();
        final Address address = customer.getAddressById(id);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultBillingAddress.ofAddress(address)));
    }

    public static CompletionStage<Customer> setDefaultBillingAddressWithAddressId(SphereClient client, Customer customer, String addressId) {
        final Address address = customer.getAddressById(addressId);
        return client.execute(CustomerUpdateCommand.of(customer, SetDefaultBillingAddress.ofAddress(address)));
    }
    public static Customer createCustomer(@RequestParam String firstName, String lastName, String email, String password, SphereClient client) throws InterruptedException, ExecutionException {
        return CustomerService.createCustomer(client, firstName, lastName, email, password)
                .thenComposeAsync(customerSignInResult ->
                        CustomerService.createEmailVerificationToken(client, customerSignInResult.getCustomer(), 5))
                .thenComposeAsync((token ->CustomerService.verifyEmail(client, token))).toCompletableFuture().get();
    }
}