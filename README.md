# Velocity-backend

Project
Velocity Incentro slot 1

Commercetools docs 
https://docs.commercetools.com/

Merchant Center
https://mc.commercetools.com/

Github account
https://github.com/commercetools

Impex
https://impex.commercetools.com/

Support
https://jira.commercetools.com/

Status
https://status.commercetools.com/

Java Code overview
https://commercetools.github.io/

SDK's overview
https://docs.commercetools.com/software-development-kits

Postman library
https://github.com/commercetools/commercetools-postman-collection

Vue.js voorbeeld project 
https://github.com/commercetools/sunrise-spa

Bitbucket Spring example
https://bitbucket.org/bitbucketpipelines/example-heroku-deploy-spring-boot/src/master/

Bitbucket Pipeline
https://bitbucket.org/atlassian/google-app-engine-deploy/src/0.5.1/

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.2.2.RELEASE/reference/htmlsingle/#boot-features-security)
* [GCP Support](https://cloud.spring.io/spring-cloud-gcp/reference/html/)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)
* [GCP Samples](https://github.com/spring-cloud/spring-cloud-gcp/tree/master/spring-cloud-gcp-samples)

### Installation
- Clone/download the project to your device
- Create an [api-client](https://docs.commercetools.com/getting-started) in the project Merchant Center and save the credentials on your device
- Add a dev.properties in src/main/resources with the credentials like this:

	{prefix}.projectKey=velocity-incentro-slot-1
	
	{prefix}.clientId=XXXXXXX
	
	{prefix}.clientSecret=XXXXXXX
	
	{prefix}.authUrl=https://auth.sphere.io
	
	{prefix}.apiUrl=https://api.sphere.io 

	
 (the prefix should match the value in Java/com/incentro/ClientService.java with the SphereClientConfig.ofProperties(...))

- application.properties
	-http.auth.tokenName=AUTH_API_KEY
	-http.auth.tokenValue=XXXXXXX

# How to run the project
-   Open your terminal 
-   Change directory to velocity
-   mvn clean install
-   mvn appengine:run

# Deploy to project
-   Set version number in pom.xml! example: <deploy.version>003</deploy.version>
-   Open your terminal 
-   Change directory to velocity
-   mvn clean install
-   mvn appengine:deploy

# Stream cloud log
-   gcloud app logs tail

# Swagger-ui
-	http://localhost:8080/swagger-ui.html